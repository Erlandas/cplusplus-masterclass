#pragma once
class Mystring
{
private:
	char* str;	//	Pointer to char[] that holds a C-style string

public:
	Mystring();							// No-args constructor
	Mystring(const char* s);			// Overloaded constructor
	Mystring(const Mystring& source);	// Copy constructor
	~Mystring();						// Destructor

	void display() const;
	int get_length() const;				// Getters
	const char* get_str() const;

};

