#include <iostream>
#include <cstring>
#include <cctype>
#include "Mystring.h"
#pragma warning ( disable : 4996 )

// No-args constructor
Mystring::Mystring()
    : str{ nullptr } {
    str = new char[1];
    *str = '\0';
}

// Overloaded constructor
Mystring::Mystring(const char* s)
    : str{ nullptr } {
    if (s == nullptr) {
        str = new char[1];
        *str = '\0';
    }
    else {
        str = new char[std::strlen(s) + 1];
        std::strcpy(str, s);
    }
}

// Copy constructor
Mystring::Mystring(const Mystring& source)
    : str{ nullptr } {
    str = new char[std::strlen(source.str) + 1];
    std::strcpy(str, source.str);
    std::cout << "Copy constructor used" << std::endl;

}

// Move constructor
Mystring::Mystring(Mystring&& source)
    :str(source.str) {
    source.str = nullptr;
    std::cout << "Move constructor used" << std::endl;
}

// Destructor
Mystring::~Mystring() {
    delete[] str;
}

// Copy assignment operator
Mystring& Mystring::operator=(const Mystring& rhs) {
    std::cout << "Using copy assignment" << std::endl;

    if (this == &rhs)
        return *this;
    delete[] str;
    str = new char[std::strlen(rhs.str) + 1];
    std::strcpy(str, rhs.str);
    return *this;
}

// Move assignment operator
Mystring& Mystring::operator=(Mystring&& rhs) {
    std::cout << "Using move assignment" << std::endl;
    if (this == &rhs)
        return *this;
    delete[] str;
    str = rhs.str;
    rhs.str = nullptr;
    return *this;
}

// Display method
void Mystring::display() const {
    std::cout << str << " : " << get_length() << std::endl;
}

// length getter
int Mystring::get_length() const { return std::strlen(str); }

// string getter
const char* Mystring::get_str() const { return str; }


// Equality
bool operator==(const Mystring& lhs, const Mystring& rhs) {
    return (std::strcmp(lhs.str, rhs.str) == 0);
}

// Make lowercase
Mystring operator-(const Mystring& obj) {
    /*
    1.  Create buffer area allocate new array of characters in that buffer area +1 for terminator \0
    2.  Copy in to the buffer from obj (that function copies 1 character at a time)
    3.  Loop through for each character (iteration) mkae lower case character
    4.  Mystring temp will initialize from buffer
    5.  Delete buffer as Mystring its already initialized from buffer and we dont need it anymore
    6.  Return created object
    */
    char* buff = new char[std::strlen(obj.str) + 1];
    std::strcpy(buff, obj.str);
    for (size_t i = 0; i < std::strlen(buff); i++)
        buff[i] = std::tolower(buff[i]);
    Mystring temp{ buff };
    delete[] buff;
    return temp;
}

// Concatenation
/*
1.  Allocate the space, bigg enough to hold both those strings and terminator (+1) \0
2.  Copy first one over in to buffer (lhs = Left Hand Side)
3.  Then concatinate in to the end of buffer from (rhs = Right Hand Side) object
4.  Create our object that we are going to return now
5.  Delete the buffer (dealocates meemory prevents memory leaks)
6.  Return created object from a buffer
*/
Mystring operator+(const Mystring& lhs, const Mystring& rhs) {
    char* buff = new char[std::strlen(lhs.str) + std::strlen(rhs.str) + 1];
    std::strcpy(buff, lhs.str);
    std::strcat(buff, rhs.str);
    Mystring temp{ buff };
    delete[] buff;
    return temp;
}