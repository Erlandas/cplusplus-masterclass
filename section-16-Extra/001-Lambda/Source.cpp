// Lambdas
/*
	From C++11
	Lambdas are anonymous functions in C++.

	Syntax :
		auto some_variable_name [Capture list] (parameter list) -> int {
			body of the function
		};
		-> :	little arrow it's optional when the return type is obvious, we dont
				need to supply this arrow nor a return type

		Capture list : By default we can't use any variables in side the body of a lambda.
			But that's exactly what capture list is for. We can capture specific variables
			by supplying their names in a comma seperated list:
			---[my_variable, some_other_variable]
			NOTE: This will just copy the values, like a parameter passed by value. If we want
			to pass a variable by reference, we supply a "&" beside its name:
			---[&my_variable, &some_other_variable]
*/
#include <iostream>
#include <iomanip>

void ruler() {
	std::cout << std::setfill('-') << std::setw(40) << "" << std::endl;
}

int main() {

	// Basic lambda no parameters
	auto say_hello_world = []() {
		std::cout << "Hello World!" << std::endl;
	};
	say_hello_world();
	ruler();
	// Lambda with parameters
	// To note in this case return type is optional compiler will figure it out
	// auto add_lambda = [](int a, int b) -> int {
	auto add_lambda = [](int a, int b) {
		return a + b;
	};
	std::cout << "Returned result : " << add_lambda(2, 5) << std::endl;
	ruler();
	// Using capture list by value, passed variable is READ ONLY
	int any_var{ 25 };
	auto double_value_lambda = [any_var]() {
		return any_var * 2;
	};
	std::cout << "Double value -> " << double_value_lambda() << std::endl;
	ruler();

	// Using capture list by refference
	int index{ 10 };
	std::cout << "Before lambda call by Reference -> index = " << index << std::endl;
	auto increment_lambda = [&index]() {
		index++;
	};
	increment_lambda();
	std::cout << "After lambda call by Reference -> index = " << index << std::endl;
	ruler();

	// [=] means for compiler to look at all of the variables that are used
	// inside body of lambda and add them automatically CAPTURES BY VALUE
	// [&] captures any variables by REFERENCE
	// we can mix [&, x] -> means capture anything by reference but x by value
	// [=, &x] everything by value but x by reference
	int x{ 1 };
	int y{ 2 };
	auto sum = [=]() {
		return x + y;
	};
	std::cout << "Sum of X and Y => " << sum() << std::endl;
	ruler();

	return 0;
}