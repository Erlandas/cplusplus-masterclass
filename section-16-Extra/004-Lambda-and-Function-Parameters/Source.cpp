/*
We can pass lambda as argument to other function
All variables in lambdos scope where lambda exists
*/

#include <iostream>
#include <functional>

void perform_operation(std::function<void()> f) {
	f();
}

int main() {

	int x{ 100 };

	auto func = [&]() {
		x++;
	};

	std::cout << "X before function call => " << x << std::endl;
	perform_operation(func);
	std::cout << "X after function call => " << x << std::endl;


	return 0;
}