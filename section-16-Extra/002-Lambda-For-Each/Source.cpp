// Lambdas
/*
	From C++11
	Lambdas are anonymous functions in C++.

	Syntax :
		auto some_variable_name [Capture list] (parameter list) -> int {
			body of the function
		};
		-> :	little arrow it's optional when the return type is obvious, we dont
				need to supply this arrow nor a return type

		Capture list : By default we can't use any variables in side the body of a lambda.
			But that's exactly what capture list is for. We can capture specific variables
			by supplying their names in a comma seperated list:
			---[my_variable, some_other_variable]
			NOTE: This will just copy the values, like a parameter passed by value. If we want
			to pass a variable by reference, we supply a "&" beside its name:
			---[&my_variable, &some_other_variable]
*/
#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
#include <iomanip>

void ruler() {
	std::cout << std::setfill('-') << std::setw(40) << "" << std::endl;
}

int main() {

	/*
	An example of using a Lambda with for_each and a vector
	we could easily write a separate function "Add(int i)", but the
	Lambda is much more concise. it is a function defined in the middle
	of the codem exactly where it is needed.
	*/

	// Include 'vector' for vector
	std::vector<int> arr{ 1,2,3,4,5 };
	int total{ 0 };

	// include 'algorithm' for for_each
	std::for_each(begin(arr), end(arr),
		[&](int x) {
		total += x;
	});

	std::cout << "Total of {1, 2, 3, 4, 5} => " << total << std::endl;
	ruler();

	return 0;
}