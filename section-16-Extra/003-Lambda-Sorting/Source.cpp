#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>

void ruler() {
	std::cout << std::setfill('-') << std::setw(40) << "" << std::endl;
}

struct Point {
	double x, y;

	Point() {
		x = (rand() % 10000) - 5000;
		y = (rand() % 10000) - 5000;
	}

	void print() {
		std::cout << "[" << x << ", " << y << "]";
	}
};

int main() {

	/*
	We can use lambda with many std::algorithms.
	This is an example of using a lambda as the 
	comparison in a call to sort std::vector
	*/

	int count{ 100 };
	std::vector<Point> points;
	for (int i{ 0 }; i < count; i++) {
		points.push_back(Point());
	}

	std::cout << "Unsorted: " << std::endl;
	for (int i{ 0 }; i < 10; i++) {
		points.at(i).print();
	}

	std::sort(points.begin(), points.end(),
		[](const Point& a, const Point& b) -> bool {
			return (a.x * a.x) + (a.y * a.y) < (b.x * b.x) + (b.y * b.y);
		});

	std::cout<< std::endl << std::endl << "Sorted: " << std::endl;
	for (int i{ 0 }; i < 10; i++) {
		points.at(i).print();
	}

	return 0;
}