#include <iostream>
#include <string>
#include "Player.h"
#include "Other_class.h"
#include "Friend_class.h"

/*
FRIEND:
    1.  A function or class that has access to private class member
    2.  And, that function of or class is NOT a member of the class it is accessing

FUNCTION:
    1.  Can be regular non-member function
    2.  Can be member methods of another class

CLASS
    1.  Another class can have access to private class members

FRIENDSHIP MUST BE GRANTED NOT TAKEN
    1.  Declared explicitly in the class that is granting friendship
    2.  Declared in the function prototype with keyword friend
*/

void display_player(Player& p) {
    std::cout << p.name << std::endl;
    std::cout << p.health << std::endl;
    std::cout << p.xp << std::endl;
}

using namespace std;

int main() {

    Player hero{ "Hero", 100, 35 };
    display_player(hero);

    Other_class other;
    other.display_player(hero);

    Friend_class friend_class;
    friend_class.set_hero_name(hero, "SUPER HERO");
    friend_class.display_player(hero);

    return 0;
}