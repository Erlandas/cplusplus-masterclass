#include "Player.h"

/*
We increment num_players when we use constructor 
And we decrement num_players when we use destructor
*/

// Initialise static variable here
int Player::num_players{ 0 };

Player::Player(std::string name_val, int health_val, int xp_val)
	: name{ name_val }, health{ health_val }, xp{ xp_val } {
	++num_players;
}

Player::Player(const Player& source)
	: Player{ source.name, source.health, source.xp } {
}

Player::~Player() {
	--num_players;
}

// Writing static method does not contain word static
// its already defined as static method in .h file
int Player::get_num_players() {
	return num_players;
}