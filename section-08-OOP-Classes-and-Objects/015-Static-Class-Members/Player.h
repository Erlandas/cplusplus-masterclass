#pragma once
#include <string>

class Player
{
private:
	static int num_players; // Declare static variables here
	std::string name;
	int health;
	int xp;

public:
	std::string get_name() const { return name; };
	int get_health() const { return health; };
	int get_xp() const { return xp; };
	Player(std::string name_val = "None", int health_val = 0, int xp_val = 0);
	//Copy constructor
	Player(const Player& source);
	//Destructor
	~Player();

	// Static methods have access only to static members of a class
	static int get_num_players();
};

