#include <iostream>
#include "Player.h"

using namespace std;

void display_active_players() {
	cout << "Active players: " << Player::get_num_players() << endl;
}

int main()
{
	// use debugger from here line 13
	display_active_players();
	Player hero{ "hero" };
	display_active_players();

	{	// Local block
		Player erlandas{ "Erlandas" };
		display_active_players();
	}
	display_active_players();

	Player* enemy = new Player("Enemy", 100, 100);
	display_active_players();
	delete enemy;
	display_active_players();

	return 0;
}

