// Move Constructor 
/*
WHEN IS IT USEFUL?
    1.  Sometimes copy constructo are called many times automatically due to the 
        copy semantic of C++
    2.  Copy constructor doing deep copying can have a significant performance bottleneck
    3.  C++11 introduced move semantics and the move constructor
    4.  Move constructor moves an object rather than copy it
    5.  Optional but recomended when you have a raw pointer
    6.  Copy elision - C++ may optimize copying away completely
        (RVO - Return Value Optimization)
*/
#include <iostream>
#include <vector>

using namespace std;

class Move {
private:
    int* data;
public:
    void set_data_value(int d) { *data = d; }
    int get_data_value() { return *data; }
    // Constructor
    Move(int d);
    // Copy Constructor
    Move(const Move& source);
    // Move Constructor
    Move(Move&& source) noexcept;
    // Destructor
    ~Move();
};

Move::Move(int d) {
    data = new int;
    *data = d;
    cout << "Constructor for: " << d << endl;
}

// Copy ctor
Move::Move(const Move& source)
    : Move{ *source.data } {
    cout << "Copy constructor  - deep copy for: " << *data << endl;
}

//Move ctor
Move::Move(Move&& source) noexcept
    : data{ source.data } {
    source.data = nullptr;
    cout << "Move constructor - moving resource: " << *data << endl;
}

Move::~Move() {
    if (data != nullptr) {
        cout << "Destructor freeing data for: " << *data << endl;
    }
    else {
        cout << "Destructor freeing data for nullptr" << endl;
    }
    delete data;
}

int main() {
    vector<Move> vec;

    vec.push_back(Move{ 10 });

    vec.push_back(Move{ 20 });
    vec.push_back(Move{ 30 });
    vec.push_back(Move{ 40 });
    vec.push_back(Move{ 50 });
    vec.push_back(Move{ 60 });
    vec.push_back(Move{ 70 });
    vec.push_back(Move{ 80 });


    return 0;
}