// Copy Constructor - Deep Copy
/*
1.  Create a copy of the pointed-to data
2.  Each copy will have a pointer to unique storage in the heap
3.  Deep copy when you have a raw pointer as a class data member
*/
#include <iostream>

using namespace std;

class Deep {
private:
    int* data;
public:
    void set_data_value(int d) { *data = d; }
    int get_data_value() { return *data; }
    // Constructor
    Deep(int d);
    // Copy Constructor
    Deep(const Deep& source);
    // Destructor
    ~Deep();
};

Deep::Deep(int d) {
    data = new int;
    *data = d;
}

Deep::Deep(const Deep& source)
    : Deep{ *source.data } {
    cout << "Copy constructor  - deep copy" << endl;
}

Deep::~Deep() {
    delete data;
    cout << "Destructor freeing data" << endl;
}

void display_deep(Deep s) {
    cout << s.get_data_value() << endl;
}

int main() {

    Deep obj1{ 100 };
    display_deep(obj1);

    Deep obj2{ obj1 };

    obj2.set_data_value(1000);

    return 0;
}