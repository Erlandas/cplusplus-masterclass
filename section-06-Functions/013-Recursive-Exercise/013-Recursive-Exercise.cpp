/*
This exercise will create a program that calculates the total amount of money
that will be accumulated if you start with a penny and double it everyday for n number of days
*/
#include <iostream>
#include <iomanip>
using namespace std;

double a_penny_doubled_everyday(int, double amount = 0.01);

void amount_accumulated(int days) {
    double total_amount = a_penny_doubled_everyday(days);
    cout << "If I start with a penny and doubled it every day for 25 days, I will have $" << setprecision(10) << total_amount;
}

double a_penny_doubled_everyday(int n, double amount) {
    if (n <= 1)
        return amount;
    return a_penny_doubled_everyday(--n, amount * 2);
}

int main() {
    amount_accumulated(25);
    cout << endl;
    return 0;
}
