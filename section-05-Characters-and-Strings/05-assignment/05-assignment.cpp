#include <iostream>
#include <string>

using namespace std;

int main()
{
    /**
     Write a C++ program that displays a Letter Pyramid from a user-provided std::string.
     Prompt the user to enter a std::string and then from that string display a Letter Pyramid as follows:
     It's much easier to understand the Letter Pyramid given examples.
     If the user enters the string "ABC", then your program should display:

      A
     ABA
    ABCBA
    */

    string input {};
    cout << "Enter something: ";
    cin >> input;

    size_t len = 2 * input.length() - 1; // get max length for the last printed line
    size_t no_of_chars = input.length();

    for (size_t i = 0; i < no_of_chars; i++)
    {
        size_t sub_len = 2 * i + 1;

        string spaces((len - sub_len) / 2, ' '); // get spaces needed

        string sub1 = input.substr(0, i + 1);
        string sub2 = string(sub1.rbegin(), sub1.rend()).substr(1, i);

        cout << "PRINT : " << sub1 << " - " << sub2 << endl;

        string net = spaces + sub1 + sub2 + spaces;

        cout << net << endl;

    }

    /**EXAMPLE
    // Letter Pyramid

    #include <iostream>
    #include <string>


    int main()
    {
        std::string letters{};

        std::cout << "Enter a string of letters so I can create a Letter Pyramid from it: ";
        getline(std::cin, letters);

        size_t num_letters = letters.length();

        int position {0};

        // for each letter in the string
        for (char c: letters) {

            size_t num_spaces = num_letters - position;
            while (num_spaces > 0) {
                std::cout << " ";
                --num_spaces;
            }

            // Display in order up to the current character
            for (size_t j=0; j < position; j++) {
                std::cout << letters.at(j);
            }

            // Display the current 'center' character
            std::cout << c;

            // Display the remaining characters in reverse order
            for (int j=position-1; j >=0; --j) {
                // You can use this line to get rid of the size_t vs int warning if you want
                auto k = static_cast<size_t>(j);
                std::cout << letters.at(k);
            }

            std::cout << std::endl; // Don't forget the end line
            ++position;
        }

        return 0;
    }
    */

    cout << endl;
    return 0;
}

