#include <iostream>

class DivisionByZeroException : public std::exception {
public:
	DivisionByZeroException() noexcept = default;
	~DivisionByZeroException() = default;
	virtual const char* what() const noexcept {
		return "Division by zero not allowed";
	}
};

void divide(int a, int b) {
	if (b == 0) {
		throw DivisionByZeroException();
	}
	double result{ static_cast<double>(a) / b };
	std::cout << "Result of " << a << " / " << b << " is " << result << std::endl;
}

int main() {

	try {
		divide(10, 0);
	}
	catch (DivisionByZeroException& ex) {
		std::cout << ex.what() << std::endl;
	}

	return 0;
}