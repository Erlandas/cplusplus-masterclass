// Stack Unwinding
#include <iostream>

void func_a();
void func_b();
void func_c();

void func_a() {
    std::cout << "Starting func_a ON STACK" << std::endl;
    func_b();
    std::cout << "Ending func_a OFF STACK" << std::endl;
}

void func_b() {
    std::cout << "Starting func_b ON STACK" << std::endl;
    try {
        func_c();
    }
    catch (int& ex) {
        std::cout << "Caught error in func_b OFF STACK" << std::endl;
    }
    std::cout << "Ending func_b, func_c OFF STACK" << std::endl;
}

void func_c() {
    std::cout << "Starting func_c ON STACK" << std::endl;
    throw 100;
    std::cout << "Ending func_c OFF STACK" << std::endl;
}

int main() {

    std::cout << "Starting main ON STACK" << std::endl;
    try {
        func_a();
    }
    catch (int& ex) {
        std::cout << "Caught error in main" << std::endl;
    }
    std::cout << "Finishing main OFF STACK" << std::endl;
    return 0;
}