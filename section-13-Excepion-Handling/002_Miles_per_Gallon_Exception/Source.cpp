#include <iostream>

int main() {
	int miles{};
	int gallons{};
	double miles_per_gallon{};

	std::cout << "Enter the miles driven: ";
	std::cin >> miles;
	std::cout << "Enter the gallons used: ";
	std::cin >> gallons;

	// try code first that potentialy can cause an exception
	try {
		if (gallons == 0) {
			throw gallons; // we can throw anything
		}
		miles_per_gallon = static_cast<double>(miles) / gallons;
		std::cout << "Result: " << miles_per_gallon << std::endl;
	}
	catch (int& ex) {
		std::cerr << "Sorry, you can't divide by zero" << std::endl;
	}

	std::cout << "Bye" << std::endl;

	return 0;
}