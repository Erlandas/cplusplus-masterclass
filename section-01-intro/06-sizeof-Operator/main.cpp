#include <iostream>
#include <climits>      // make sure you include climits for integer types
                        // Similar information for floating point numbers
                        // is contained in <cfloat>

using namespace std;

int main() {

    cout << "sizeof information" << endl;
    cout << "========================" << endl;
    
    cout << "char: " << sizeof(char) << " bytes." << endl;
    cout << "int : " << sizeof(int) << " bytes." << endl;
    cout << "unsigned int: " << sizeof(unsigned int) << " bytes." << endl;
    cout << "short: " << sizeof(short) << " bytes." << endl;
    cout << "long: " << sizeof(long) << " bytes." << endl;
    cout << "long long: " << sizeof(long long) << " bytes." << endl;
    
    cout << "========================" << endl;

    cout << "float: " << sizeof(float) << " bytes." << endl;
    cout << "double: " << sizeof(double) << " bytes." << endl;
    cout << "long double: " << sizeof(long double) << " bytes." << endl;
//
//
    // use values defined in <climits>
    cout << "========================" << endl;
    
    cout << "Minimum values:" << endl;
    cout << "char: " << CHAR_MIN << endl;
    cout << "int: " << INT_MIN << endl;
    cout << "short: " << SHRT_MIN << endl;
    cout << "long: " << LONG_MIN << endl; 
    cout << "long long: " << LLONG_MIN << endl;
//     
    cout << "========================" << endl;
    
    cout << "Maximum values:" << endl;
    cout << "char: " << CHAR_MAX << endl;
    cout << "int: " << INT_MAX << endl;
    cout << "short: " << SHRT_MAX << endl;
    cout << "long: " << LONG_MAX << endl; 
    cout << "long long: " << LLONG_MAX << endl;
//    
    // sizeof can also be used with variable names
    cout << "========================" << endl;
    
    cout << "sizeof using variable names" << endl;
    int age {21};
    cout << "age is " << sizeof(age) << " bytes." << endl;
    // or
     cout << "age is " << sizeof age << " bytes." << endl;
     
    double wage { 22.24};
    cout << "wage is " << sizeof(wage) << " bytes." << endl;
    // or
    cout << "wage is " << sizeof wage << " bytes." << endl;


    return 0;
}

/*
 * sizeof information
========================
char: 1 bytes.
int : 4 bytes.
unsigned int: 4 bytes.
short: 2 bytes.
long: 8 bytes.
long long: 8 bytes.
========================
float: 4 bytes.
double: 8 bytes.
long double: 16 bytes.
========================
Minimum values:
char: -128
int: -2147483648
short: -32768
long: -9223372036854775808
long long: -9223372036854775808
========================
Maximum values:
char: 127
int: 2147483647
short: 32767
long: 9223372036854775807
long long: 9223372036854775807
========================
sizeof using variable names
age is 4 bytes.
age is 4 bytes.
wage is 8 bytes.
wage is 8 bytes.
*/