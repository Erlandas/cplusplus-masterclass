# include <iostream>

using namespace std;

// Global variables automatically initialized to 0
int age {18}; //Global variable

int main() {
    
    int age {16}; //Local variable shadows global

    cout << age << endl;
    
    return 0;
}