/* 
 * The Challenge:
 * 
    Erlanda's Carpet Cleaning Service
    Charges:
        $25 per small room
        $35 per large room
    Sales tax rate is 6%
    Estimates are valid for 30 days

    Prompt the user for the number of small and large rooms they would like cleaned
    and provide an estimate such as:
     
Estimate for carpet cleaning service
Number of small rooms: 3
Number of large rooms: 1
Price per small room: $25
Price per large room: $35
Cost : $110
Tax: $6.6
===============================
Total estimate: $116.6
This estimate is valid for 30 days
*/

#include <iostream>

using namespace std;

int main() {
    
    cout << "Erlanda's Carpet Cleaning Service" << endl << endl ;
    
    cout << "How many small rooms would you like cleaned? " ;
    int amount_small_rooms { 0 } ;
    cin >> amount_small_rooms ;
    cout << "How many large rooms would you like cleaned? " ;
    int amount_large_rooms { 0 } ;
    cin >> amount_large_rooms ;
    
    const double price_per_small_room { 25.0 } ;
    const double price_per_large_room { 35.0 } ;
    const double sales_tax_rate { 0.06 } ;
    const int price_validity { 30 } ;
    const double price_for_rooms { ( price_per_large_room * amount_large_rooms ) + ( price_per_small_room * amount_small_rooms ) } ;
    const double tax_charge { price_for_rooms * sales_tax_rate } ;
    
    cout << "\nEstimate for carpet cleaning service" << endl ;
    cout << "Number of small rooms: " << amount_small_rooms << endl ;
    cout << "Number of large rooms: " << amount_large_rooms << endl ;
    cout << "Price per small room: $" << price_per_small_room << endl ;
    cout << "Price per large room: $" << price_per_large_room << endl ;
    cout << "Cost: $" << price_for_rooms << endl ;
    cout << "Tax: $" << tax_charge << endl ;
    cout << "===================================" << endl ;
    cout << "Total estimate: $" << ( price_for_rooms + tax_charge ) << endl ;
    cout << "This estimate is valid " << price_validity << " days" << endl << endl ;
    
 
    cout << endl;
    return 0;
}