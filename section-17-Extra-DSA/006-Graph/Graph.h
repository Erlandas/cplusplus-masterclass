#pragma once
#include <iostream>
#include <iomanip>

template <int CAPACITY, bool IS_DIRECTED , bool IS_WEIGHTED >
class Graph {
private:

	int num_of_nodes{ CAPACITY };
	bool directed{ IS_DIRECTED };
	bool weighted{ IS_WEIGHTED };
	int adjacency_matrix[CAPACITY][CAPACITY]{};
	bool is_set[CAPACITY][CAPACITY]{false};

public:

	/*
	Method that adds node to adjacency matrix
	conditions checked:
		1: if node is at legal bounds
		2: if correct value is added
		3: if graph is undirected to add adjacent node
	*/
	bool add_edge(int &position_x, int &position_y, int weight = 1) {
		
		//Check if node at legal bounds of array
		if (position_x >= num_of_nodes && position_y >= num_of_nodes) {
			std::cout << "out of bounds" << std::endl;
			return false;
		}

		//Check if value is legal
		/*
		Unweighted graph initial value to represent node is 1
		0 represents that there is no connection
		*/
		int value_to_add{ weight };
		if (!weighted && value_to_add != 1 || value_to_add == 0) {
			value_to_add = 1;
		}

		/*
		Add node to 2D array at supplied coordinates
		Conditional statement checks if graph is undirected
		if graph is directed node is not added
		*/
		adjacency_matrix[position_x][position_y] = value_to_add;
		is_set[position_x][position_y] = true;
		if (!directed) {
			adjacency_matrix[position_y][position_x] = value_to_add;
			is_set[position_y][position_x] = true;
		}
		return true;
		
	};

	void print_statistics() {
		std::cout << std::boolalpha;
		std::cout << std::setw(10) << "Capacity :" << std::setw(7) << num_of_nodes << std::endl;
		std::cout << std::setw(10) << "Directed :" << std::setw(7) << directed << std::endl;
		std::cout << std::setw(10) << "Weighted :" << std::setw(7) << weighted << std::endl;
	}

	void print_graph() {

		for (int i{ -1 }; i < num_of_nodes; i++) {
			if (i == -1) {
				std::cout << std::setw(5) << "";
			}
			else {
				std::cout << std::setw(5) << i;
			}
		}
		std::cout << std::endl << std::setfill('-') << std::setw((num_of_nodes+1)*5) << "" << std::setfill(' ') << std::endl ; 

		for (int row{ 0 }; row < num_of_nodes; row++) {
			std::cout << std::setw(3) << row << " |";
			for (int col{ 0 }; col < num_of_nodes; col++) {
				std::cout << std::setw(5) << adjacency_matrix[row][col];
			}
			std::cout << std::endl;
		}
	}

	bool has_edge(int &pos_x, int &pos_y) {

		if ((pos_x < num_of_nodes && pos_x >= 0) && (pos_y < num_of_nodes && pos_y >= 0)) {
			return is_set[pos_x][pos_y];
		}
		return false;
	}

	bool display_adj_sites(int &site) {
		if (site < num_of_nodes && site >= 0) {

			std::cout << "Adjacent sites for site [" << site << "] is : ";
			for (int i{ 0 }; i < num_of_nodes; i++) {
				if (is_set[site][i]) {
					std::cout << i << ", ";
				}
			}
			std::cout << std::endl;
			return true;
		}
		return false;
	}

	bool display_closest(int &site) {
		if (site < num_of_nodes && site >= 0) {
			int closest{-1};
			int closest_value{ INT16_MAX };
			for (int i{ 0 }; i < num_of_nodes; i++) {
				if (adjacency_matrix[site][i] > 0) {
					if (adjacency_matrix[site][i] < closest_value) {
						closest = i;
						closest_value = adjacency_matrix[site][i];
					}
				}
			}
			std::cout << "Closest site to site [" << site << "] is site [" << closest << "] with weight of : " << closest_value;
			std::cout << std::endl;
			return true;
		}
		return false;
	}

	void print_edges() {			
			for (int row{ 0 }; row < num_of_nodes ; row++) {
				std::cout << "Locations for [" << row << "] : ";
				for (int col{ 0 }; col < num_of_nodes ; col++) {
					if (is_set[row][col]) {
						std::cout << col << ", ";
					}
				}
				std::cout << std::endl;
			}

	}


};