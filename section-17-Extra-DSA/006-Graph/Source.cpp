#include <iostream>
#include <iomanip>
#include "Graph.h"
#include <fstream>

bool initialise_graph_weighted(std::string file_path);
bool initialise_graph_unweighted(std::string file_path);
void main_menu();
void display_menu();
void site_to_site();
void adj_sites();
void closest();
Graph<5, false, true> g;

int main() {

	std::string file_name{"graph_test.txt"};
	initialise_graph_weighted(file_name);
	main_menu();
	return 0; 
}

/*
supplied text file
Weight, position x, position y
4 0 1
10 0 3
7 0 4
5 1 2
8 2 3
3 3 4

This method initialises graph from a text file
	1: Declare needed variables
	2: Open file stream
	3: Check if file open if not return FALSE
	4: Run while end of file not reached
	5: Add edges while loop runs
	6: Close file
	7: Return true
*/
bool initialise_graph_weighted(std::string file_path) {

	int weight{};
	int pos_x{};
	int pos_y{};
	std::ifstream in_file;

	in_file.open(file_path);
	if (!in_file.is_open()) {
		return false;
	}

	while (!in_file.eof()) {
		in_file >> weight >> pos_x >> pos_y;
		g.add_edge(pos_x, pos_y, weight);
	}

	in_file.close();

	return true;
}

/*
Takes same steps as for weighted graph 
Just no weight supplied
*/
bool initialise_graph_unweighted(std::string file_path) {
	int pos_x{};
	int pos_y{};
	std::ifstream in_file;

	in_file.open(file_path);
	if (!in_file.is_open()) {
		return false;
	}

	while (!in_file.eof()) {
		in_file >> pos_x >> pos_y;
		g.add_edge(pos_x, pos_y);
	}

	in_file.close();

	return true;
}

void main_menu() {
	bool is_running{ true };
	int option{};
	while (is_running) {
		display_menu();
		std::cin >> option;

		if (option == 7) {
			is_running = false;
			std::cout << "Exiting...." << std::endl;
			continue;
		}

		switch (option) {
			case 1: {
				site_to_site();
				break;
			}
			case 2: {
				adj_sites();
				break;
			}
			case 3: {
				closest();
				break;
			}
			case 4: {
				g.print_edges();
				break;
			}
			case 5: {
				g.print_statistics();
				break;
			}
			case 6: {
				g.print_graph();
				break;
			}
			default: {
				std::cout << "Plese enter from menu choices" << std::endl;
				break;
			}
		}

		system("PAUSE");
		system("cls");
	}
}

void closest() {
	int site{};
	std::cout << "Enter a site : ";
	std::cin >> site;
	if (!g.display_closest(site)) {
		std::cout << "Site can not be found" << std::endl;
	}
}

void adj_sites() {
	int site{};
	std::cout << "Enter a site : ";
	std::cin >> site;
	if (!g.display_adj_sites(site)) {
		std::cout << "Site can not be found" << std::endl;
	}
}

void site_to_site() {
	int site_one{};
	int site_two{};
	std::cout << "Enter both sites separated by space : ";
	std::cin >> site_one >> site_two;
	if (g.has_edge(site_one, site_two)) {
		std::cout << "Edge exists between site [" << site_one << "] and site [" << site_two << "]." << std::endl;
	}
	else {
		std::cout << "One of the sites can not be found or doesnt have an edge" << std::endl;
	}
}

void display_menu() {
	std::string menu{ "\tSmple Graph"
					"\n----------------------------------------------------"
					"\n1.\tSearch site to site"
					"\n2.\tDisplay adjacent sites to given site"
					"\n3.\tDisplay closest site to given site"
					"\n4.\tPrint all locations and adjacent locations to every location"
					"\n5.\tPrint Statistics"
					"\n6.\tPrint adjacency matrix"
					"\n7.\tEXIT"
					"\n> "};
	std::cout << menu;
}
