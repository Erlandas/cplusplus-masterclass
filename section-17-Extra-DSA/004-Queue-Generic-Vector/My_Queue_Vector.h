#pragma once
#include <vector>
/*
---Standard Queue Operations---

1.	enqueue(value)
	Element are added from one end

2.	dequeue()
	Element are removed from one end

3.	is_empty()
	Check if queue is empty
	Return boolean

4.	size()
	Returns size of a queue

5.	peek()
	Returns element at the end of a queue without changing queue

NOTE:
	Assuming in this implementation we adding elements at the front
	Removing at the Back
	--> |1|2|3|4|...|n -->

*/

template <typename T>
class My_Queue_Vector {
private:
	std::vector<T> my_queue_vector;
public:
	void enqueue(T value) {
		auto iterator = my_queue_vector.begin();
		my_queue_vector.insert(iterator, value);
	}

	T dequeue() {
		if (is_empty()) {
			return -1;
		}
		T return_value = my_queue_vector.back();
		my_queue_vector.pop_back();
		return return_value;
	}

	T peek() {
		if (is_empty()) {
			return -1;
		}
		return my_queue_vector.back();
	}

	int size() {
		return my_queue_vector.size();
	}

	bool is_empty() {
		return my_queue_vector.empty();
	}

	// Testing purposes
	void display() {
		std::cout << "In display" << std::endl;
		std::cout << "My Vector Queue : [ ";
			for (auto val : my_queue_vector) {
				std::cout << val << ", ";
			}
		std::cout << " ]" << std::endl;
	}

};
