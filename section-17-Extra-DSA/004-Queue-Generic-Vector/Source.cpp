#include <iostream>
#include "My_Queue_Vector.h"

int main() {
	std::cout << std::boolalpha;

	My_Queue_Vector<int> vq;
	std::cout << "empty? : " << vq.is_empty() << std::endl;
	vq.display();
	vq.enqueue(1);
	std::cout << "empty? : " << vq.is_empty() << std::endl;
	vq.display();
	std::cout << "peek : " << vq.peek() << std::endl;
	std::cout << "dequeue : " << vq.dequeue() << std::endl;
	vq.display();

	vq.display();
	for (int i{ 0 }; i < 20; i++) {
		vq.enqueue(i);
		vq.display();
	}

	vq.display();
	for (int i{ 0 }; i < 25; i++) {
		std::cout << "peek : " << vq.peek() << std::endl;
		std::cout << "dequeue : " << vq.dequeue() << std::endl;
		vq.dequeue();
		vq.display();
	}

	return 0;
}