#include <iostream>
#include <iomanip>
#include "My_Queue_Array.h"

int main() {
	std::cout << std::boolalpha;
	My_Queue_Array<std::string, 2> str_q;
	str_q.display();
	str_q.enqueue("ONE");
	str_q.display();
	str_q.enqueue("TWO");
	str_q.display();
	str_q.enqueue("THREE");
	str_q.display();

	My_Queue_Array<int> q;
	std::cout << "Peek() : " << ((q.peek() == NULL)?-1:q.peek()) << std::endl;

	std::cout << "Is empty?? : " << q.is_empty() << std::endl;
	std::cout << "Is full?? : " << q.is_full() << std::endl;

	q.display();
	q.enqueue(5);
	q.display();
	q.enqueue(15);
	q.display();
	q.enqueue(25);
	q.display();

	std::cout << "Is empty?? : " << q.is_empty() << std::endl;
	std::cout << "Is full?? : " << q.is_full() << std::endl;
	for (auto i : {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}) {
		q.enqueue(i);
		q.display();
		std::cout << "Is empty?? : " << q.is_empty() << std::endl;
		std::cout << "Is full?? : " << q.is_full() << std::endl;
	}

	std::cout << "\n\n---------------------------------------------------------------------------" << std::endl;
	q.display();
	while (!q.is_empty()) {
		
		std::cout << "Peek() : " << q.peek() << std::endl;
		std::cout << "Is empty?? : " << q.is_empty() << std::endl;
		std::cout << "Is full?? : " << q.is_full() << std::endl;
		q.dequeue();
		q.display();
	}

	return 0;
}