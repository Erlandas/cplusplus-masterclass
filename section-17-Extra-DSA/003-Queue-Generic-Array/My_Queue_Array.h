/*
---Standard Queue Operations---

1.	enqueue(value)
	Element are added from one end
	returns boolean indicating succesful operation

2.	dequeue()
	Element are removed from one end

3.	is_full()
	Check if queue is full
	Return boolean

4.	is_empty()
	Check if queue is empty
	Return boolean

5.	size()
	Returns size of a queue

6.	peek()
	Returns element at the end of a queue without changing queue

NOTE:
	Assuming in this implementation we adding elements at the front
	Removing at the Back
	--> |1|2|3|4|...|n -->

*/

#pragma once
#include <iostream>

template < typename T, int CAPACITY = 10 >
class My_Queue_Array {
private:
	T my_queue[CAPACITY]{};
	int end;

public:
	My_Queue_Array() {
		this->end = -1 ;
		
	}

	bool enqueue(T value) {
		if (is_full()) {
			return false;
		}
		// Do shifting
		for (int i{ end }; i >= 0; i--) {
			my_queue[i + 1] = my_queue[i];
		}
		my_queue[0] = value;
		end++;
		return true;

	}

	T dequeue() {
		if (is_empty()) {
			return NULL;
		}
		T temp_var = my_queue[end];
		my_queue[end] = NULL;
		end -= 1;
		return temp_var;
	}

	T peek() const {
		if (is_empty()) {
			return NULL;
		}
		return my_queue[end];
	}

	bool is_full() const {
		return (end == CAPACITY - 1);
	}

	bool is_empty() const {
		return (end == -1);
	}

	int size() const {
		return end;
	}

	// Testing purposes
	void display() {
		std::cout << "Queue : [ ";
		for(int i{0} ; i <= end ; i++){
			std::cout << my_queue[i] << ", ";
		}
		std::cout << " ]" << std::endl;
	}
};
