#include <iostream>
#include <iomanip>
#include "My_Stack.h"

template <typename T>
void display(My_Stack<T> s) {
	std::cout << "[ ";
	while (!s.is_empty()) {
		T elem = s.peek();
		s.pop();
		std::cout << elem << " ";
	}
	std::cout << "]" << std::endl;
}

int main() {

	My_Stack<int> stk;
	std::cout << std::boolalpha;
	std::cout << stk.is_empty() << std::endl;
	std::cout << stk.is_full() << std::endl;

	// Testing push
	display(stk);
	std::cout << "\nTesting peek " << stk.peek() << std::endl;
	std::cout << "\nTesting size " << stk.size() << std::endl;
	for (int i{ 0 }; i < 14; i++) {
		stk.push(i);
		display(stk);
	}
	std::cout << "\nTesting peek " << stk.peek() << std::endl;

	// Testing pop
	std::cout << "\nTesting size " << stk.size() << std::endl;
	for (int i{ 0 }; i < 14; i++) {
		stk.pop();
		display(stk);
	}
	std::cout << "\nTesting size " << stk.size() << std::endl;


	return 0;
}