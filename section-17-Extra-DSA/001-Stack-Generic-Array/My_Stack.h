/*
---Standard Stack Operations---

1.	push()
	Place an item onto the stack. If there is no place for
	new item, stack is in overflow state. Returns boolean

2.	pop()
	Return the item at the top of the stack nd then remove it. Returns -1 if element not found

3.	is_empty()
	Tell if the stack is empty or not returns boolean

4.	is_full()
	Tell if the stack is full or not returns boolean

5.	peek() or top()
	return item at top position without modifying stack

6.	size()
	Get the number oif items in a stack

*/
#pragma once
// Template parameters requires type of stack and size default size 10
template <typename T, int MAX_SIZE = 10>
class My_Stack{
private:

	T my_stack[MAX_SIZE]{};
	int top{ -1 };

public:

	bool push(T value) {
		if (is_full()) {
			return false;
		}
		top += 1;
		my_stack[ top ] = value;
		return true;
	}

	T pop() {
		if (is_empty()) {
			return NULL;
		}
		T temp_var = my_stack[top];
		my_stack[top] = NULL;
		top -= 1;
		return temp_var;
	}

	T peek() {
		if (is_empty()) {
			return NULL;
		}
		return my_stack[ top ];
	}

	int size() {
		return ( top + 1 );
	}

	bool is_empty() {
		return ( top == -1 );
	}

	bool is_full() {
		return ( top == MAX_SIZE - 1 );
	}

};











