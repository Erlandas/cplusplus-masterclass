/*
---Standard Stack Operations---

1.	push()
	Place an item onto the stack. If there is no place for
	new item, stack is in overflow state. Returns boolean

2.	pop()
	Return the item at the top of the stack nd then remove it. Returns -1 if element not found

3.	is_empty()
	Tell if the stack is empty or not returns boolean

4.	peek() or top()
	return item at top position without modifying stack

5.	size()
	Get the number oif items in a stack

*/

#pragma once
#include <iostream>
#include <vector>

template <typename T>
class My_Stack_Vector {
private:
	std::vector<T> my_stack;
public:

	T pop() {
		if (is_empty()) {
			return NULL;
		}
		T temp_element = peek();
		my_stack.pop_back();
		return temp_element;
	}

	void push(T value) {
		my_stack.push_back(value);
	}

	T peek() {
		if (is_empty()) {
			return -1;
		}
		return my_stack.at( size() -1 );
	}

	int size() {
		return my_stack.size();
	}

	bool is_empty() {
		return (my_stack.size() == 0);
	}

	//testing purposes
	void display() {
		std::cout << "[ ";
		for (auto val : my_stack) {
			std::cout << val << ", ";
		}
		std::cout << " ]" << std::endl;
	}
};