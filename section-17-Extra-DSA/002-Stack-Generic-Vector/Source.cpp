#include <iostream>
#include <iomanip>
#include "My_Stack_Vector.h"

int main() {
	My_Stack_Vector<int> stk;
	std::cout << std::boolalpha;
	std::cout << "Peek : " << stk.peek() << std::endl;
	std::cout << "Size : " << stk.size() << std::endl;
	std::cout << "Empty? : " << stk.is_empty() << std::endl;
	for (auto i : { 1,2,3,4,5,6,7,8,9 }) {
		stk.push(i);
		stk.display();
		std::cout << "Peek : " << stk.peek() << std::endl;
		std::cout << "Size : " << stk.size() << std::endl;
		std::cout << "Empty? : " << stk.is_empty() << std::endl;
	}
	std::cout << "\nEmpty? : " << stk.is_empty() << std::endl;
	while (!stk.is_empty()) {
		stk.pop();
		stk.display();
		std::cout << "Peek : " << stk.peek() << std::endl;
		std::cout << "Size : " << stk.size() << std::endl;
		std::cout << "Empty? : " << stk.is_empty() << std::endl;
	}
	std::cout << "\nEmpty? : " << stk.is_empty() << std::endl;

	
	return 0;
}