#include <iostream>
#include <map>

/*
---Part 1:---

Problem:
	Given a string, convert the string to palindrome without any modifications 
	like adding a character, removing a character, replacing a character etc.

Examples:
	Input : "mdaam"
	Output : "madam" or "amdma"

	Input : "abb"
	Output : "bab"

	Input : "hello"
	Output : "No Palindrome"

Solution Steps:
	1.	Count occurrences of all characters and store to map<char, occurences>.
	2.	Count odd occurrences. If this count is greater than 1 or is equal to 1 and length of the string is even 
		then obviously palindrome cannot be formed from the given string.
	3.	Initialize two empty strings firstHalf and secondHalf.
	4.	Traverse the map. For every character with count as count, attach count/2 characters to end of firstHalf 
		and beginning of secondHalf.
	5.	Finally return the result by appending firstHalf and secondHalf

---Part 2---

	If given string has lower case and upper case letters that means even if palindrome
	could be made from a string it won't be as a != A, 
	so make lower case all of the letters or upper case

	Also some cleaning suppose to be made for non alpha characters such as: , . : : ? !

	Using references to save some memory
*/

std::string rearange_palindrome(std::string str);
void clean_string(std::string &str);

int main() {
	rearange_palindrome(std::string("Hello"));
	std::cout << rearange_palindrome(std::string("abb")) << std::endl;
	std::cout << rearange_palindrome(std::string("M,.;daam")) << std::endl;
	std::cout << rearange_palindrome(std::string("C++")) << std::endl;
	std::cout << rearange_palindrome(std::string("Carreac")) << std::endl;
	std::cout << rearange_palindrome(std::string("a;")) << std::endl;
	std::cout << rearange_palindrome(std::string("rere!!!d")) << std::endl;
	std::cout << rearange_palindrome(std::string("lmmada")) << std::endl;
	return 0;
}

// -----Part 1-----
std::string rearange_palindrome(std::string str) {

	std::cout << "Given string before clean [ " + str + " ]." << std::endl;

	// Removes unvanted characters from a string
	// Makes string UPPER case
	clean_string(str);

	std::cout << "Given string after clean [ " + str + " ]." << std::endl;

	// Store counts of characters to a map <letter ( char ) , occurences ( int )>
	std::map<char, int> count_map{};
	for (auto const &val : str) {
		count_map[val]++;
	}

	//TESTING
	//for (auto val : count_map) {
	//	std::cout << "first : " << val.first << " last : " << val.second << std::endl;
	//}
	
	// find number of odd elements linear time complexity O(n)
	// also keep odd element
	int odd_count{};
	char odd_char{};
	for (auto const &val : count_map) {
		if (val.second % 2 != 0) {
			odd_count++;
			odd_char = val.first;
		}
	}

	// Conditional statement that checks if palindrome can be made
	if (odd_count > 1 || odd_count == 1 && str.length() % 2 == 0) {
		return "Palindrome from [ " + str + " ] can not be formed.";
	}

	// Build two strings first half and second half
	std::string first_half{ "" };
	std::string second_half{ "" };
	for (auto const &letter : count_map) {

		//Create a string that holds current letter count/2 occurence times
		//Example 4/2 = 2, first = a , s = aa
		std::string s(letter.second / 2, letter.first);

		//Build first and second half
		first_half = s + first_half;
		second_half = second_half + s;
	}

	return "Palindrome from [ " + str + " ] is [ " + (first_half + odd_char + second_half) + " ].";
}

// -----Part 2-----
void clean_string(std::string& str) {
	std::string temp_str{ "" };
	for(auto &val : str){
		if (isalpha(val)) {
			temp_str += toupper(val);
		}
	}
	str = temp_str;
}