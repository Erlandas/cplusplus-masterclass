/*
	---THE OVERRIDE SPECIFIER---

	1.	We can override Base class virtual function
	2.	The function signature and return must be EXACTLY the same
	3.	if they are different then we have redefinition NOT overriding
	4.	Redefinition is statically bound
	5.	Overriding is dynamically bound
	6.	C++11 provides an override specifier to have th compiler ensure overriding
*/
// Using override
#include <iostream>

class Base {
public:
    virtual void say_hello() const {
        std::cout << "Hello - I'm a Base class object" << std::endl;
    }
    virtual ~Base() {}
};

class Derived : public Base {
public:
    virtual void say_hello()  const override {             // Notice I forgot the const 
        std::cout << "Hello - I'm a Derived class object" << std::endl;
    }
    virtual ~Derived() {}
};


int main() {

    Base* p1 = new Base();      // Base::say_hello()
    p1->say_hello();

    Derived* p2 = new Derived();    // Derived::say_hello()
    p2->say_hello();

    Base* p3 = new Derived();   //  Base::say_hello()   ?????   I wanted Derived::say_hello()
    p3->say_hello();


    return 0;
}