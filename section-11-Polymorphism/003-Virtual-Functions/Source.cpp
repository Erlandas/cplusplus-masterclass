/*
	---VIRTUAL FUNCTIONS---

	1.	Redefined functions are bound statically
	2.	Overriden functions are bound dynamically
	3.	Virtual functions are OVERRIDEN
	4.	Allow us to treat all objects generally as objects of the Base/Parent class

	---DECLARING VIRTUAL FUNCTIONS---

	1.	Declare the function you want to override as virtual in the Base class
	2.	Virtual functions are virtual all the way down the hierarchy from this point
	3.	Dynamic polymorphism only via Account class pointer or reference
    4.  Override the function in the Deriver classes
    5.  Functions signature and return type must match EXACTLY
    6.  Virtual keyword not required but is best practice
    7.  If you don't priovide an overriden version it is inherited from it's base class

	example: "virtual void withdraw(double amounnt)"
*/

// Virtual functions

#include <iostream>

// This class uses dynamic polymorphism for the withdraw method
class Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Account::withdraw" << std::endl;
    }
};

class Checking : public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Checking::withdraw" << std::endl;
    }
};

class Savings : public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Savings::withdraw" << std::endl;
    }
};

class Trust : public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Trust::withdraw" << std::endl;
    }
};

int main() {
    std::cout << "\n === Pointers ==== " << std::endl;
    Account* p1 = new Account();
    Account* p2 = new Savings();
    Account* p3 = new Checking();
    Account* p4 = new Trust();

    p1->withdraw(1000);
    p2->withdraw(1000);
    p3->withdraw(1000);
    p4->withdraw(1000);


    std::cout << "\n === Clean up ==== " << std::endl;
    delete p1;
    delete p2;
    delete p3;
    delete p4;

    return 0;
}