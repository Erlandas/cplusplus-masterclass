/*
    ---What is using a class as an interface?---

    1.  An abstract class that has only pure virtual functions
    2.  These functions provide a general set of services to the user of the class
    3.  Provided as public
    4.  Each subclass is free to implement these services as needed
    5.  Every service (method) must be implemented
    6.  The services type information is strictly enforced
    7.  C++ does not provide true interfaces
    8.  We use abstract classes and pure virtual functions to achieve it
    9.  Suppose we want to be able to provide Printable support for any
        object we wish without knowing it's implementation at compile time
*/
// Interfaces - complete

#include <iostream>

//INTERFACE
class I_Printable {
    friend std::ostream& operator<<(std::ostream& os, const I_Printable& obj);
public:
    virtual void print(std::ostream& os) const = 0;
    virtual ~I_Printable() = default;
};

std::ostream& operator<<(std::ostream& os, const I_Printable& obj) {
    obj.print(os);
    return os;
}


class Account : public I_Printable {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Account::withdraw" << std::endl;
    }
    virtual void print(std::ostream& os) const override {
        os << "Account display";
    }
    virtual ~Account() {  }
};

class Checking : public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Checking::withdraw" << std::endl;
    }
    virtual void print(std::ostream& os) const override {
        os << "Checking display";
    }
    virtual ~Checking() {  }
};


class Savings : public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Savings::withdraw" << std::endl;
    }
    virtual void print(std::ostream& os) const override {
        os << "Savings display";
    }
    virtual ~Savings() {  }
};

class Trust : public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Trust::withdraw" << std::endl;
    }
    virtual void print(std::ostream& os) const override {
        os << "Trust display";
    }
    virtual ~Trust() {  }
};

class Dog : public I_Printable {
public:
    virtual void print(std::ostream& os) const override {
        os << "Woof woof";
    }
};

void print(const I_Printable& obj) {
    std::cout << obj << std::endl;
}


int main() {

    Dog* dog = new Dog();
    std::cout << *dog << std::endl;

    print(*dog);

    Account* p1 = new Account();
    std::cout << *p1 << std::endl;

    Account* p2 = new Checking();
    std::cout << *p2 << std::endl;

    //    Account a;
    //    std::cout << a<< std::endl;
    //    
    //    Checking c;
    //    std::cout << c << std::endl;
    //
    //    Savings s;
    //    std::cout << s << std::endl;
    //    
    //    Trust t;
    //    std::cout << t << std::endl;

    delete p1;
    delete p2;
    delete dog;
    return 0;
}