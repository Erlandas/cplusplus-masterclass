/*
	---VIRTUAL DESTRUCTORS---

	1.	Problem can happen when we destroy polymorphic objects
	2.	If a derived class is destroyed by deleting its storage via the base class pointer
		and the class a non-virtual destructor. then the behavior is undefined in the
		C++ standard.
	3.	Derived objects must be destroyed in the correct order starting at the correnct
		destructor

	---SOLUTION / RULE---

	If a class has virtual function
	<<ALWAYS>> provide a public virtual destructor
	If base class destructor is virtual then all derived class destructors are also virtual

    NOTE:   to observe behaviour run program with deleted virtual keyword beside destructor
            and then run with virtual attached to destructor
*/

// Virtual destructors

#include <iostream>

// This class uses dynamic polymorphism for the withdraw method
class Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Account::withdraw" << std::endl;
    }
    virtual ~Account() { std::cout << "Account:: destructor" << std::endl; }
};

class Checking: public Account  {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Checking::withdraw" << std::endl;
    }
    virtual ~Checking() { std::cout << "Checking:: destructor" << std::endl; }

};

class Savings: public Account  {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Savings::withdraw" << std::endl;
    }
    virtual ~Savings() { std::cout << "Savings:: destructor" << std::endl; }

};

class Trust: public Account  {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Trust::withdraw" << std::endl;
    }
    virtual ~Trust() { std::cout << "Trust:: destructor" << std::endl; }

};

int main() {

    std::cout << "\n === Pointers ==== " << std::endl;
    Account *p1 = new Account();
    Account *p2 = new Savings();
    Account *p3 = new Checking();
    Account *p4 = new Trust();
    
    p1->withdraw(1000);
    p2->withdraw(1000);
    p3->withdraw(1000);
    p4->withdraw(1000);
    

    std::cout << "\n === Clean up ==== " << std::endl;
    delete p1;
    delete p2;
    delete p3;
    delete p4;
        
    return 0;
}