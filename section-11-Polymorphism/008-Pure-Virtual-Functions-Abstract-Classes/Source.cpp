/*
	---Pure virtual functions and abstract classes---

	Abstract class:
	1.	Cannot instanciate objects
	2.	These classes are used as base classes in inheritance hierarchies
	3.	often referred to as Abstract Base Classes

	Concrete class
	1.	Used to instantiate objects from
	2.	All their member functions are defined

	Abstract base class
	1.	Too generic to create objects from
		(shape, employee, account, player)
	2.	Serves as parent to Derived classes that may have objects
	3.	Contains at least one pure virtual function
	4.	We can use pointers and references to dynamically refer to concrete classes
		derived from them

	Pure virtual functions
	1.	Used to make a class abstract
	2.	Specified with " = 0 " in its declaration
		( " virtual void function() = 0; " )
	3.	Typically do not provide implementation
	4.	Derived classes --<<<MUST>>>-- override the base class
	5.	If the derived class does not override then the derived class is also abstract
	6	Used when it doesn't make sense for a base class to have an implementation
		But concrete classes must implement it

	examples:
		virtual void draw() = 0 ; // shape
		virtual void defend() = 0 ; // player
*/
// Pure virtual functions and abstract base classes
#include <iostream>
#include <vector>

class Shape {			// Abstract Base class
private:
    // attributes common to all shapes
public:
    virtual void draw() = 0;	 // pure virtual function
    virtual void rotate() = 0;   // pure virtual function
    virtual ~Shape() { }
};

class Open_Shape : public Shape {    // Abstract class
public:
    virtual ~Open_Shape() { }
};

class Closed_Shape : public Shape {  // Abstract class
public:
    virtual ~Closed_Shape() { };
};

class Line : public Open_Shape {     // Concrete class
public:
    virtual void draw() override {
        std::cout << "Drawing a line" << std::endl;
    }
    virtual void rotate() override {
        std::cout << "Rotating a line" << std::endl;
    }
    virtual ~Line() {}
};

class Circle : public Closed_Shape {     // Concrete class
public:
    virtual void draw() override {
        std::cout << "Drawing a circle" << std::endl;
    }
    virtual void rotate() override {
        std::cout << "Rotating a circle" << std::endl;
    }
    virtual ~Circle() {}
};

class Square : public Closed_Shape {     // Concrete class
public:
    virtual void draw() override {
        std::cout << "Drawing a square" << std::endl;
    }
    virtual void rotate() override {
        std::cout << "Rotating a square" << std::endl;
    }
    virtual ~Square() {}
};


void screen_refresh(const std::vector<Shape*>& shapes) {
    std::cout << "Refreshing" << std::endl;
    for (const auto p : shapes)
        p->draw();
}

int main() {
    //    Shape s;
    //    Shape *p = new Shape();

    //        Circle c;
    //        c.draw();

    //    Shape *ptr = new Circle();
    //    ptr->draw();
    //    ptr->rotate();

    Shape* s1 = new Circle();
    Shape* s2 = new Line();
    Shape* s3 = new Square();

    std::vector<Shape*> shapes{ s1,s2,s3 };

    //    for (const auto p: shapes) 
    //        p->draw();

    screen_refresh(shapes);

    delete s1;
    delete s2;
    delete s3;

    return 0;
}