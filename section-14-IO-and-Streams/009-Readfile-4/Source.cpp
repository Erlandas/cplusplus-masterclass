/*
Read and display Shakespeare Sonnet 18 poem using unformated get
*/

#include <iostream>
#include <fstream>
#include <string>

int main() {

	std::ifstream in_file;
	in_file.open("poem.txt");
	if (!in_file.is_open()) {
		std::cerr << "problem opening file" << std::endl;
		return 1;
	}

	char c{};
	while (in_file.get(c)) {
		std::cout << c;
	}

	std::cout << std::endl;
	in_file.close();
	return 0;
}