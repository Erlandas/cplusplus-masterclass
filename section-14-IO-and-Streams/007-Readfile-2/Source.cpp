/*
	Read file 1
	Test for file open and simple read of 3 data items

	---Input files (fstream and ifstream)---

	fstream and ifstream are commonly used for input files

	1.	#include <fstream>
	2.	Declare an fstream or ifstream object
	3.	Connect it to a file on your file system (opens it for reading)
	4.	Read data from the file via the stream
	5.	Close the stream
*/

#include <iostream>
#include <fstream>  // include to work with files
#include <iomanip>

int main() {

	std::ifstream in_file;
	std::string line;
	int num;
	double total;

	in_file.open("test.txt");
	if (!in_file.is_open()) {
		std::cerr << "Problem opening file" << std::endl;
		return 1;
	}
	
	//while (!in_file.eof()) // read while not EOF -> End Of File
	//{
	//	in_file >> line >> num >> total;
	//	std::cout << std::setw(10) << std::left << line
	//			<< std::setw(10) << num
	//			<< std::setw(10) << total
	//			<< std::endl;
	//}
	std::cout << std::setw(40) << std::setfill('*') << "" << std::setfill(' ') << std::endl;
	while (in_file >> line >> num >> total) // if something fails while wont execute further
	{
		std::cout << std::setw(10) << std::left << line
			<< std::setw(10) << num
			<< std::setw(10) << total
			<< std::endl;
	}
	in_file.close();	// ALWAYS CLOSE THE FILE AFTER USING IT!!!

	return 0;
}