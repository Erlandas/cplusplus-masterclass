/*
	---Output Files (fstream and ofstream)---

	fstream and ofstream are commonly used for output files

	---Steps---
	1.	#include <fstream>
	2.	Declare an fstream or ofstream object
	3.	Connect it to a file on your file system (opens it for writing)
	4.	Write data to the file via the stream
	5.	Close the stream

	---Notes---
	1.	Output files will be created if they don't exist
	2.	Output files will be overwritten (truncated)) by default
	3.	Can be opened so that new writes append
	4.	Can be open in text or binary modes

	---Flags---
	1.	truncate (discard contents) when opening
	std::ofstream out_file {"myfile.txt", std::ios::trunc};

	2.	append on each write
	std::ofstream out_file {"myfile.txt", std::ios::app};

	3.	seek to end of stream when opening
	std::ofstream out_file {"myfile.txt", std::ios::ate};

*/

#include <iostream>
#include <fstream>
#include <string>

int main() {

	std::ofstream out_file{ "output.txt", std::ios::app};
	if (!out_file) {
		std::cout << "Error creating file" << std::endl;
		return 1;
	}

	std::string line;
	std::cout << "Enter something to write to the file: ";
	std::getline(std::cin, line);
	out_file << line << std::endl;

	out_file.close();
	return 0;
}