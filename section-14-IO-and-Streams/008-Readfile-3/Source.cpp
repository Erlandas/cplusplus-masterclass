/*
Read and display Shakespeare Sonnet 18 poem using getline
*/

#include <iostream>
#include <fstream>
#include <string>

int main() {

	std::ifstream in_file;
	in_file.open("poem.txt");
	if (!in_file.is_open()) {
		std::cerr << "problem opening file" << std::endl;
		return 1;
	}
	std::string line;
	while (std::getline(in_file, line)) {
		std::cout << line << std::endl;
	}

	in_file.close();
	return 0;
}